const express = require('express')
const path = require('path');
// const dataProccess = require('./dataProccess');
const app = express();
app.use(express.static(path.join(__dirname, '../build')));
app.use(express.json());
// dataProccess(app);
app.get('*', function(req, res) {
    res.redirect('/index.html');
});
app.listen(80);