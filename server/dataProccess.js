var mongoose = require('mongoose');
var paypal = require('paypal-rest-sdk');
var paymentList = require('./paymentList');
var status = require('./status');

paypal.configure({
  'mode': 'live', //sandbox or live
  'client_id': 'Aexc3bcY5X-PelgGkSaFySgwDtb0DW3ZhcQdJNKpP29giWRdi3CWuSnQKvysTW-6VfUN-37JbOogrYu1',
  'client_secret': 'EEdK6iSpaTOx2wR9JXPQbuzL_rxm0tfP1VTvoSiEmYceOlQyZPApwY3lzyn1qWU9rJyih06lga80xZR6'
});
mongoose.connect('mongodb://root:zxcvbnm16@ds151626.mlab.com:51626/isaacwebsite',{ useNewUrlParser: true });
const Schema = mongoose.Schema;
const SponserSchema = new Schema({
  id:String,
  name:String,
  url:String,
});
const SponserModel = mongoose.model('sponser', SponserSchema);
const PaymentSchema = new Schema({
  paypal_id:String,
  paid:Boolean,
  id:String,
  name:String,
  url:String,
});
const PaymentModel = mongoose.model('payment', PaymentSchema);



var create_payment_json = {
    "intent": "sale",
    "payer": {
        "payment_method": "paypal"
    },
    "redirect_urls": {
        "return_url": "http://react.isaac.taipei/payment/success",
        "cancel_url": "http://react.isaac.taipei/payment/failed"
    },
    "transactions": [{
        "amount": {
            "currency": "TWD",
            "total": "10.00"
        },
        "description": "This is the payment description."
    }]
};

module.exports = function (app){

	app.post('/paypal/',(req,res)=>{
    if(!req.body.id){
      res.send(JSON.stringify());
      return false;
    }

    SponserModel.findOne({id:req.body.id}).exec((err, stories)=>{
      if(err){
        res.send(JSON.stringify(status(status.FAILD)));
        return false;
      }
      if(stories){
        res.send(JSON.stringify(status(status.ALREADY_BUY)));
        return false;
      }
      let price = (paymentList.find((item)=>{return item[0]==req.body.id})||[])[1];
      if(!price){
        res.send(JSON.stringify(status(status.FAILD)));
        return false;
      }
      create_payment_json.transactions[0].amount.total = price.toString();

      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            console.log(error);
            res.send(JSON.stringify(status(status.FAILD)));
            return false;
        } else {
            let Pay = new PaymentModel({
              id:req.body.id,
              paypal_id:payment.id,
              paid:false,
              name:req.body.name,
              url:req.body.url
            });
            Pay.save();
            let s = status(status.SUCCESS);
            s.data = payment.links[1].href;
            res.send(JSON.stringify(s));
        }
      });

    });
	})


  app.use('/payment/successful/',(req,res,next)=>{
    PaymentModel.findOne({paypal_id:req.query.paymentId,paid:false})
    .exec((err, item)=>{
      if(!err){
        item.paid = true;
        item.save();
        new SponserModel({id:item.id,name:item.name,url:item.url}).save();
      }
    })
    next();
  })

  app.get('/sponser/',(req,res)=>{
    SponserModel.find().exec((err,item)=>{
      if(err){
        res.send(JSON.stringify(status(status.FAILD)));
        return false;
      }
      let s = status(status.SUCCESS);
          s.data = item;
          res.send(JSON.stringify(s));
    })
  })

	app.post('/getCanvasObj/',(req,res)=>{
		let Canvas = new CanvasModel({id:'1',title:'2',sponser:'3',url:'4'});  
		Canvas.save(()=>{
		CanvasModel.find().exec((err, stories)=>{
			    res.send(JSON.stringify({
			      data:stories,
			      message:"",
			      code:""
			    }))
			})
		});
	})
}