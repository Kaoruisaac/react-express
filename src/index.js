import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter,Route,Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import Layout from './layouts/layout';
import Home from './pages/Home';
import About from './pages/About';
import Website from './pages/Website';
import App from './pages/App';
import System from './pages/System';
import { createStore } from 'redux';
import combine from './reducers/';

ReactDOM.render(
		<BrowserRouter>
			<Provider store={createStore(combine)}>
				<Layout>
					<Switch>
						<Route path="/item/:id" component={Home} />
						<Route path="/payment/:id" component={Home} />
						<Route path="/about/" component={About} />
						<Route path="/website/:id" component={Website} />
						<Route path="/website/" component={Website} />
						<Route path="/app/:id" component={App} />
						<Route path="/app/" component={App} />
						<Route path="/system/:id" component={System} />
						<Route path="/system/" component={System} />

						<Route path="/" component={Home} />
					</Switch>
				</Layout>
			</Provider>
		</BrowserRouter>
	, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
