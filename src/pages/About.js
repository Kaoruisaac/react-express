import React from 'react';

class About extends React.Component {
  render() {
    return (
      <div className="aboutPage subPage">
        <div className="grid">
          <div></div>
          <div>
            <div className="container">
              <div className="title">
                <div className="portrait bgCover" style={{backgroundImage:`url('/img/portriat.jpg')`}}></div>
                <div>
                  <h1>林士薰</h1>
                  <h2>Isaac Lin</h2>
                </div>
              </div>
              <div className="content">
                <p>
                  不管去到哪間公司都是出了名的工作狂，但今年開始為了讓皮膚變好，所以正努力嘗試不熬夜的生活。
                  <br/>
                  <br/>
                  學生時期做過平面設計助理工讀，大學時期接觸網頁設計實習，加上初中開始就買書自學C++，畢業後便
                  一腳踏入前端的工作領域。
                  <br/>
                  <br/>
                  主要工作經歷從退伍開始，
                  2015~2017年在 上聯國際展覽有限公司 作網站設計師，前期公司模式較為傳統，以一年100餘個展覽網站的大量設計、切版以及與企劃單位共從策劃及執行網路行銷活動為主，後期則參與展覽網站CMS模板系統的前端開發，並引入React作為部分展覽網站的應用框架。
                  <br/>
                  <br/>
                  2017 3月 在模板系統完成後，毅然決然飛到澳洲墨爾本找前端工程師的工作，同年6月，加入位於 Melbourne Glen Waverley 的 MengoTech Group IT團隊，專職 Front-end Development，接觸的專案多元而且有趣，像是親子樂園的線上及門店售票系統、NFC感應閘門控制、清潔公司POS App、機場接送APP、生活資訊類APP平台、諸多客制網站及內部系統...等等。主要使用 Vue、Angular、ionic、Electron 實作。
                </p>
              </div>
              <div className="tagBlock">
                <span className="tag red-0">Vue 2</span>
                <span className="tag red-1">Nuxt</span>
                <span className="tag red-2">Ionic 3</span>
                <span className="tag red-3">Angular 6</span>
                <span className="tag red-4">Electron</span>
                <span className="tag red-5">Android</span>
                <span className="tag red-0">Jersey</span>
                <span className="tag red-1">Express</span>
                <span className="tag green-0">Javascript</span>
                <span className="tag green-1">Typescript</span>
                <span className="tag green-2">Java</span>
                <span className="tag green-3">CSS / SASS</span>
                <span className="tag green-4">HTML</span>
                <span className="tag green-5">PHP</span>
                <span className="tag green-0">MongoDB</span>
                <span className="tag green-1">Node js</span>

                <span className="tag blue-0">Illustrator</span>
                <span className="tag blue-1">Photoshop</span>
                <span className="tag blue-2">Lightroom</span>
                <span className="tag blue-3">Animate CC</span>

                <span className="tag gray-0">Git</span>
                <span className="tag gray-1">Docker</span>
                <span className="tag gray-2">Webpack</span>
                <span className="tag gray-3">Gulp</span>
                <span className="tag gray-4">Cordova</span>
                <span className="tag gray-5">Xcode</span>
                <span className="tag gray-0">AWS EC2</span>
                <span className="tag gray-1">Azure</span>
                <span className="tag gray-2">Eclipse</span>
                <span className="tag gray-2">Android Studio</span>
              </div>
            </div>
          </div>
          <div></div>
        </div>
      </div>
    );
  }
}

export default About;