import React from 'react';
import axios from 'axios';
import MainCanvas from '../components/MainCanvas';
import SideInfo from '../components/SideInfo';
import Payment from '../components/Payment';
import { connect } from 'react-redux';
import { SHOW_MAIN,SHOW_SIDE_INFO,SHOW_PAYMENT,UPDATE_SPONSOR_LIST } from '../reducers/home';
import { HostUrl } from '../config';


const pageRef = ['','sideOpen','paymentOpen'];

class Home extends React.Component {
  constructor(props){
  	super(props);
    this.detectStatusByPath(this.props.location.pathname);
    this.getSponser();
  }
  componentDidUpdate(prevProps){
    if (this.props.location !== prevProps.location) {
    	this.detectStatusByPath(this.props.location.pathname);
    }
  }
  detectStatusByPath(path){
    if(path.match('item')){
      setTimeout(()=>{
        this.props.dispatch({type:SHOW_SIDE_INFO,id:this.props.match.params.id});
      },0)
    }else if(path.match('payment')){
      setTimeout(()=>{
        this.props.dispatch({type:SHOW_PAYMENT,id:this.props.match.params.id});
      },0);
    }else{
      setTimeout(()=>{
        this.props.dispatch({type:SHOW_MAIN});
      },0)
    }
  }
  getSponser(){
      axios.get(HostUrl+"sponser/").then(res=>{
        if(res.data.code==1000){
          this.props.dispatch({type:UPDATE_SPONSOR_LIST,data:res.data.data});
        }else{
          alert(res.data.msg)
        }
      })
   }
  render() {
    return (
    	<div className="homePage">
		    <div className={`grid ${pageRef[this.props.pageIndex]}`}>
		      <MainCanvas></MainCanvas>
		      <SideInfo></SideInfo>
		      <Payment></Payment>
		    </div>
		</div>
    );
  }
}

export default connect((state)=>{return{
    pageIndex: state.home.pageIndex
}})(Home);