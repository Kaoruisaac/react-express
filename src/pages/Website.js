import React from 'react';
import { GET_INFO } from '../reducers/project';
import websiteRef from '../store/websiteRef';
import { connect } from 'react-redux';

class Website extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      isOpen:false,
    }
  }
  componentDidMount(){
    this.detectStatusById(this.props.match.params.id);
  }
  componentDidUpdate(prevProps){
    if (this.props.location !== prevProps.location) {
      this.detectStatusById(this.props.match.params.id);
    }
  }
  detectStatusById(id){
    if(!id){
      this.setState({isOpen:false});
      return false;
    }
    let data = websiteRef.find(item=>item.id==id)
    if(!data){
      this.routeTo('');
      return false;
    }
    this.props.dispatch({type:GET_INFO,data});
    this.setState({isOpen:true});
  }
  itemLoop(){
    return websiteRef.map((item,index)=>
      <article style={{backgroundImage:`url(${item.img})`}}
               className={item.isBig?'large':''}
               key={index}
               onClick={()=>this.routeTo(item.id)}></article>
      )
  }
  tagsLoop(tags){
     return tags.map((item,index)=>
       <span className={`tag gray-${(index%5)+1}`} key={index}>{item}</span>
      )
  }
  routeTo(id){
    this.props.history.push('/website/'+id);
  }
  render() {
    return (
      <div className="websitePage subPage">
        <div className="grid">
          <div></div>
          <div>
            <div className="container">
              <div className="catGrid">
                {this.itemLoop()}
              </div>
            </div>
          </div>
          <div></div>
        </div>
        <div className={`float ${this.state.isOpen?'active':''}`}>
          <div className="bg" onClick={()=>this.routeTo('')}></div>
          <div className="side">
            <article>
              <div>
                <h2>{this.props.info.title}</h2>
                {this.props.info.url&&
                  <p><i className="icon-link"></i><a href={this.props.info.url} target="_blank">{this.props.info.url}</a></p>
                }
                {this.props.info.git&&
                  <p><i className="icon-github"></i><a href={this.props.info.git} target="_blank">{this.props.info.git}</a></p>
                }
                <div className="content">
                  <p v-if="info.year">{this.props.info.year}</p>
                  <p v-if="info.description">{this.props.info.description}</p>
                  <p style={{marginTop:'20px'}}>
                    {this.tagsLoop(this.props.info.tags||[])}
                  </p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    );
  }
}

export default connect((state)=>{
  return {
    info:state.project.info
  }
})(Website);