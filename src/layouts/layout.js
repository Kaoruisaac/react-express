import MenuHeader from '../components/menuHeader';
import { string } from 'prop-types';
import React from 'react';
import { withRouter } from 'react-router-dom'

class Layout extends React.Component {
  constructor(props){
    super(props);
    this.state={
      isDark:false,
    }
  }
  componentDidMount(){
      this.detectStatusByPath(this.props.location.pathname);
  }
  componentDidUpdate(prevProps){
    if (this.props.location !== prevProps.location) {
      this.detectStatusByPath(this.props.location.pathname);
    }
  }
  detectStatusByPath(path){
    if(path.match('website')||path.match('app')||path.match('system')){
      this.setState({isDark:true});
    }else{
      this.setState({isDark:false});
    }
  }
  render(){
    return (
      <div id="app">
        <div className={`wrapper ${this.state.isDark?'dark':'white'}`}>
          <MenuHeader/>
          <div className="main">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
};
Layout.propTypes = {
  title: string,
};
export default withRouter(Layout);
