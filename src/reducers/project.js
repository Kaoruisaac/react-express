import itemsRef from '../store/itemsRef'
const GET_INFO = "GET_INFO";

const dataInit={
	info:{
		id:null,
		title:null,
		titleEn:null,
		year:null,
		des:null,
		desEn:null,
		img:null,
		git:null,
		url:null,
		tags:null,
		isBig:false,
	}
}

const project = (state=dataInit,action)=>{
	switch (action.type){
		case GET_INFO:
			state.info = action.data;
		break;
	}
	return Object.assign({},state);
};

export default project;
export {GET_INFO};