import { combineReducers } from 'redux';
import home from './home';
import project from './project';

const combine = combineReducers({home,project});

export default combine;