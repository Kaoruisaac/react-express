import itemsRef from '../store/itemsRef'
const SHOW_MAIN = "SHOW_MAIN";
const SHOW_SIDE_INFO = "SHOW_SIDE_INFO";
const SHOW_PAYMENT = "SHOW_PAYMENT";
const UPDATE_SPONSOR_LIST = "UPDATE_SPONSOR_LIST";

const dataInit={
	pageIndex:0,
	info:{
		id:null,
		type:null,
		title:null,
		description:null,
		price:null,
		img:null,
	},
	sponsorInfo:{
		name:null,
		url:null,
	},
	sponsorInfoList:[],
}

function fillInfo(state,action){
	for(let group of itemsRef){
        let itemId = group.items.find(id=>action.id==id); 
        if(itemId){
          state.info = group;
          state.info.id = action.id;
          break;
        }        
    }
    if(action.id){
    	state.sponsorInfo = state.sponsorInfoList.find(s=>s.id==action.id)||dataInit.sponsorInfo;
    }
}

const home = (state=dataInit,action)=>{
	switch (action.type){
		case SHOW_MAIN:
			state.pageIndex = 0;
		break;
		case SHOW_SIDE_INFO:
			state.pageIndex = 1;
			fillInfo(state,action);
		break;
		case SHOW_PAYMENT:
			state.pageIndex = 2;
			fillInfo(state,action);
		break;
		case UPDATE_SPONSOR_LIST:
			state.sponsorInfoList = action.data;
			if(state.info.id){
		    	state.sponsorInfo = state.sponsorInfoList.find(s=>s.id==state.info.id)||dataInit.sponsorInfo;
			}
		break;
		
	}
	return Object.assign({},state);
};

export default home;
export {SHOW_MAIN,SHOW_SIDE_INFO,SHOW_PAYMENT,UPDATE_SPONSOR_LIST};