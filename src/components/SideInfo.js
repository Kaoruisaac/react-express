import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

class SideInfo extends React.Component {
  constructor(props) {
    super(props);
    this.routeTo = this.routeTo.bind(this);
  }
  routeTo(id){
  	this.props.history.push('/payment/'+id);
  }
  render() {
    return (
    	<div className="sideInfo">
		    <article>
		      <div>
		        <div className="portrait">
		          <img src={this.props.info.img}/>
		        </div>
		        <div className="content">
		          <h1>{this.props.info.title} {this.props.sponsorInfo.name?' - '+this.props.sponsorInfo.name:''}</h1>
		          <p dangerouslySetInnerHTML={{__html: this.props.info.description}}></p>
		        </div>
		        {this.props.sponsorInfo.url &&
    				<div className="promo">
			          <h3>{this.props.sponsorInfo.name} 買下了此物件，快點去他們的網站看看吧!</h3>
			          <p><a href={this.props.sponsorInfo.url} target="_blank"><i className="icon-link"/>{this.props.sponsorInfo.url}</a></p>
			        </div>
		        }
		        {!this.props.sponsorInfo.name &&
			        <div className="bottom" v-if="!sponsorInfo.name">
			          <div className="btnBlock">
			            <span className="btn large" onClick={()=>this.routeTo(this.props.info.id)}>贊助 $ {this.props.info.price} TWD </span>
			          </div>
			          <p>
			            對，你沒看錯，這明明是個人作品集網站 <br/>
			            但ISAAC非常的厚顏無恥的讓你可以透過小額的贊助 <br/>
			            讓這些物件掛上你的大名。
			          </p>
			        </div>
			    }
		      </div>
		    </article>
		</div>
    );
  }
}
export default connect((state)=>{
	return {
		info:state.home.info,
		sponsorInfo:state.home.sponsorInfo,
	}
})(withRouter(SideInfo));