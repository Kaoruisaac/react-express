import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import { INFO_UPDATE } from '../reducers/home';
import itemsRef from '../store/itemsRef'

class MainCanvas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	title:null,
    }
    this.hover = this.hover.bind(this);
    this.sentId = this.sentId.bind(this);
    this.routeTo = this.routeTo.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
  }
  hover(id){
  	let title = null;
  	if(!id){
  		this.setState({title});
        return false;
  	}
  	for(let group of itemsRef){
        let itemId = group.items.find(tid=>id==tid); 
        if(itemId){
          title = group.title;
          let sponsorName = (this.props.sponsorInfoList.find(s=>s.id==itemId)||{}).name;
          if(sponsorName){
	          title += ' - '+sponsorName;
          }
          this.setState({title});
          return false;
        }        
    }
  }
  sentId(id){
  	this.props.history.push('/item/'+id);
  }
  routeTo(path){
  	this.props.history.push(path);
  }
  changeTitle(title){
    this.setState({title});
  }
  render() {
    return (
		<div className="mainCanvas">
		    <div className="fullFrame">
		      <div className="bg" onClick={()=>this.routeTo('/')} onMouseOver={()=>this.hover(null)}></div>
		      <div className="canvas">
		        <div className="area a7">
		          <div className="object a7_1 ground"><img alt="7-1" src="/img/home/7-1.png"/></div>
		          <div className="object a7_2" onClick={()=>this.sentId('a7_2')} onMouseOver={()=>this.hover('a7_2')}><img alt="7-2" src="/img/home/7-2.png"/></div>
		          <div className="object a7_3" onClick={()=>this.sentId('a7_3')} onMouseOver={()=>this.hover('a7_3')}><img alt="7-3" src="/img/home/7-3.png"/></div>
		          <div className="treeGroup2 a7_4">
		            <div className="object t3" onClick={()=>this.sentId('a7_4_t3')} onMouseOver={()=>this.hover('a7_4_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a7_4_t2')} onMouseOver={()=>this.hover('a7_4_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a7_4_t2')} onMouseOver={()=>this.hover('a7_4_t2')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		          <div className="treeGroup2 a7_5">
		            <div className="object t3" onClick={()=>this.sentId('a7_5_t3')} onMouseOver={()=>this.hover('a7_5_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a7_5_t2')} onMouseOver={()=>this.hover('a7_5_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a7_5_t1')} onMouseOver={()=>this.hover('a7_5_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		        </div>
		        <div className="area a6">
		          <div className="object a6_1 ground"><img alt="6-1" src="/img/home/6-1.png"/></div>
		          <div className="object a6_2" onClick={()=>this.sentId('a6_2')} onMouseOver={()=>this.hover('a6_2')}><img alt="6-3" src="/img/home/6-3.png"/></div>
		        </div>
		        <div className="area a1">
		          <div className="object a1_1 ground"><img alt="1-1" src="/img/home/1-1.png"/></div>
		          <div className="object a1_2" onClick={()=>this.sentId('a1_2')} onMouseOver={()=>this.hover('a1_2')}><img alt="1-2" src="/img/home/1-2.png"/></div>
		          <div className="object a1_3" onClick={()=>this.sentId('a1_3')} onMouseOver={()=>this.hover('a1_3')}><img alt="1-3" src="/img/home/1-3.png"/></div>
		          <div className="treeGroup a1_4">
		            <div className="object t3" onClick={()=>this.sentId('a1_4_t3')} onMouseOver={()=>this.hover('a1_4_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a1_4_t2')} onMouseOver={()=>this.hover('a1_4_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a1_4_t1')} onMouseOver={()=>this.hover('a1_4_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		        </div>
		        <div className="object pc" onClick={()=>this.routeTo('system')} onMouseOver={()=>this.changeTitle('System')}><img alt="pc-1" src="/img/home/pc-1.png"/></div>
		        <div className="object laptop" onClick={()=>this.routeTo('website')} onMouseOver={()=>this.changeTitle('Website')}><img alt="l-1" src="/img/home/l-1.png"/></div>
		        <div className="area a3">
		          <div className="object a3_1 ground"><img alt="3-1" src="/img/home/3-1.png"/></div>
		          <div className="treeGroup a3_2">
		            <div className="object t3" onClick={()=>this.sentId('a3_2_t3')} onMouseOver={()=>this.hover('a3_2_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a3_2_t2')} onMouseOver={()=>this.hover('a3_2_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a3_2_t1')} onMouseOver={()=>this.hover('a3_2_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		        </div>
		        <div className="area p">
		          <div className="object p_1" onClick={()=>this.routeTo('app')} onMouseOver={()=>this.changeTitle('App')}><img alt="p-1" src="/img/home/p-1.png"/></div>
		          <div className="object p_2" onClick={()=>this.sentId('p_2')} onMouseOver={()=>this.hover('p_2')}><img alt="p-2" src="/img/home/p-2.png"/></div>
		          <div className="object p_3" onClick={()=>this.sentId('p_3')} onMouseOver={()=>this.hover('p_3')}><img alt="p-3" src="/img/home/p-3.png"/></div>
		        </div>
		        <div className="area a4">
		          <div className="object a4_1 ground"><img alt="4-1" src="/img/home/4-1.png"/></div>
		          <div className="treeGroup a4_2">
		            <div className="object t3" onClick={()=>this.sentId('a4_2_t3')} onMouseOver={()=>this.hover('a4_2_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a4_2_t2')} onMouseOver={()=>this.hover('a4_2_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a4_2_t1')} onMouseOver={()=>this.hover('a4_2_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		          <div className="object a4_3" onClick={()=>this.sentId('a4_3')} onMouseOver={()=>this.hover('a4_3')}><img alt="4-2" src="/img/home/4-2.png"/></div>
		        </div>
		        <div className="area a5">
		          <div className="object a5_1 ground"><img alt="5-1" src="/img/home/5-1.png"/></div>
		          <div className="object a5_2" onClick={()=>this.sentId('a5_2')} onMouseOver={()=>this.hover('a5_2')}><img alt="5-2" src="/img/home/5-2.png"/></div>
		        </div>
		        <div className="object trem" onClick={()=>this.sentId('trem')} onMouseOver={()=>this.hover('trem')}><img alt="trem.png" src="/img/home/trem.png"/></div>
		        <div className="area a2">
		          <div className="object a2_1 ground"><img alt="2-1" src="/img/home/2-1.png"/></div>
		          <div className="treeGroup a2_2">
		            <div className="object t3" onClick={()=>this.sentId('a2_2_t3')} onMouseOver={()=>this.hover('a2_2_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a2_2_t2')} onMouseOver={()=>this.hover('a2_2_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a2_2_t1')} onMouseOver={()=>this.hover('a2_2_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		          <div className="treeGroup a2_3">
		            <div className="object t3" onClick={()=>this.sentId('a2_3_t3')} onMouseOver={()=>this.hover('a2_3_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a2_3_t2')} onMouseOver={()=>this.hover('a2_3_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a2_3_t1')} onMouseOver={()=>this.hover('a2_3_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		          <div className="treeGroup a2_4">
		            <div className="object t3" onClick={()=>this.sentId('a2_4_t3')} onMouseOver={()=>this.hover('a2_4_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a2_4_t2')} onMouseOver={()=>this.hover('a2_4_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a2_4_t1')} onMouseOver={()=>this.hover('a2_4_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		          <div className="treeGroup a2_5">
		            <div className="object t3" onClick={()=>this.sentId('a2_5_t3')} onMouseOver={()=>this.hover('a2_5_t3')}><img alt="t-3" src="/img/home/t-3.png"/></div>
		            <div className="object t2" onClick={()=>this.sentId('a2_5_t2')} onMouseOver={()=>this.hover('a2_5_t2')}><img alt="t-2" src="/img/home/t-2.png"/></div>
		            <div className="object t1" onClick={()=>this.sentId('a2_5_t1')} onMouseOver={()=>this.hover('a2_5_t1')}><img alt="t-1" src="/img/home/t-1.png"/></div>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div className="descriptionBar">
		      <p>{this.state.title}</p>
		    </div>
		</div>
    );
  }
}
export default connect((state)=>{
	return {
		sponsorInfoList:state.home.sponsorInfoList,
	}
})(withRouter(MainCanvas));