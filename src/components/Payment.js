import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { HostUrl } from '../config';

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state={
    	loading:false,
    	name:'',
    	url:'',
    }
    this.submit = this.submit.bind(this);
  }
  submit(){
  	this.setState({loading:true});
	if(this.state.name==''){
	    alert('your name can not be empty !');
	    return false;
	}
	axios.post(HostUrl+'paypal/',{
		name:this.state.name,
		url:this.state.url,
		id:this.props.info.id,
	}).then(res=>{
		if(res.data.code==1000){
		      window.location.href = res.data.data;
		}else{
		    alert(res.data.msg);
		  	this.setState({loading:false});
		}
	}).catch(()=>{
		alert("Server Error!");
	  	this.setState({loading:false});
	})
  }
  render() {
    return (
    	<div className="payment">
		    <article>
		      <form>
		        <div className="inputGroup">
		          <input type="text" name="" value={this.state.name} onChange={(event)=>this.setState({name:event.target.value})}/>
		          <label>你的名字 ( 限20字 )</label>
		        </div>
		        <div className="inputGroup">
		          <input type="text" name="" value={this.state.url} onChange={(event)=>this.setState({url:event.target.value})}/>
		          <label>推廣網址</label>
		        </div>
		        <div className="totalBlock">
		          <div>
		            <h2>應付金額</h2>
		            <span>$ {this.props.info.price} TWD</span>
		          </div>
		          <span className="btn large" onClick={this.submit}>下一步</span>
		        </div>
		      </form>
		    </article>
		    <div className="loadingCover"></div>
		</div>
    );
  }
}
export default connect((state)=>{
	return {
		info:state.home.info,
	}
})(Payment);