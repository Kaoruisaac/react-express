import {Link} from 'react-router-dom'
import Logo from './Logo';
import React from 'react';
import { connect } from 'react-redux';

class menuHeader extends React.Component {
  constructor(props){
    super(props);
    this.state={
      isOpen:false
    }
  }
  render(){
    return (
      <header className={this.props.pageIndex==2?'payment':''}>
        {this.state.isOpen&&
          <div class="bg" onClick={()=>this.setState({isOpen:false})}></div>
        }
        <div>
          <div className="logoFlex">
            <div><Link to="/"><Logo/></Link></div>
          </div>
          <div className={`MenuBlock ${this.state.isOpen?'active':''}`} >
            <ul onClick={()=>this.setState({isOpen:false})}>
              <li><Link to="/about">關於我</Link></li>
              <li><Link to="/website">網站</Link></li>
              <li><Link to="/system">系統</Link></li>
              <li><Link to="/app">APP</Link></li>
              <li><a href="https://gitlab.com/Kaoruisaac" target="_blank" rel="noopener noreferrer"><i className="icon-github"></i></a></li>
              <li><a href="https://www.linkedin.com/in/isaac-lin-05576b9a/" target="_blank" rel="noopener noreferrer"><i className="icon-linkedin"></i></a></li>
              <li><a href="mailto:kaoruisaac@gmail.com" target="_blank" rel="noopener noreferrer"><i className="icon-mail"></i></a></li>
            </ul>
          </div>
          <nav className={this.state.isOpen?'active':''} onClick={()=>this.setState({isOpen:!this.state.isOpen})}>
            <b></b>
            <b></b>
            <b></b>
          </nav>
        </div>
      </header>
    )
  }
};

export default connect((state)=>{
  return {
    pageIndex: state.home.pageIndex,
  }
})(menuHeader);
