FROM node:10.15.3

ENV NODE_ENV=production
ENV HOST 0.0.0.0

RUN mkdir -p /app
COPY package*.json ./app/
WORKDIR /app
RUN npm install
WORKDIR /
COPY . /app
WORKDIR /app
# Expose the app port
EXPOSE 80

RUN npm run build
CMD ["npm", "run","express"]
